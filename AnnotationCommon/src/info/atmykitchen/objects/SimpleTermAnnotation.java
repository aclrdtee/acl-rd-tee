/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.atmykitchen.objects;

/**
 *
 * @author Behrang QasemiZadeh <me at atmykitchen.info>
 */
public class SimpleTermAnnotation implements Comparable<SimpleTermAnnotation> {

    private String annClass;
    private String termString;

    public SimpleTermAnnotation(String annClass, String termString) {
        this.annClass = annClass;
        this.termString = termString;
    }

    public String getAnnClass() {
        return annClass;
    }

    public String getTermString() {
        return termString;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SimpleTermAnnotation) {
            SimpleTermAnnotation oS = (SimpleTermAnnotation) o;
            if (this.annClass.equals(oS.annClass) && this.termString.equals(oS.termString)) {
                return true;
            }
        }
        return false;

    }

    @Override
    public int compareTo(SimpleTermAnnotation t) {

        int cmpStr = this.termString.compareTo(t.termString);
        int cmpStr2 = this.annClass.compareTo(t.annClass);
        if (cmpStr == 0 && cmpStr2 == 0) {
            return 0;
        } else if (cmpStr != 0) {
            return cmpStr;
        } else {
            return cmpStr2;
        }
    }

    @Override
    public String toString() {
        return this.termString+"\t"+this.annClass;
    }
    
    

}
