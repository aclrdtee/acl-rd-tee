/* 
 * Copyright 2015 Behrang QasemiZadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.atmykitchen.common;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Get all the files in a folder and its sub-folders
 * @author Behrang
 */
public class GetFiles {

    private List<String> setOfFiles;

    public GetFiles() {
        setOfFiles = new ArrayList<String>();
    }

    public void getCorpusFiles(String parentDir) {
        FilenameFilter filter1 = new FilenameFilter() {

            public boolean accept(File dir, String name) {
                return !name.startsWith(".");
            }
        };

        File dir = new File(parentDir);
        if (dir.isDirectory()) {
            File[] dirs = dir.listFiles(filter1);
            for (int i = 0; i < dirs.length; i++) {
                if (dirs[i].isFile()) {
                   // if (dir.getAbsolutePath().endsWith("pdf")) {

                    setOfFiles.add(dirs[i].getAbsolutePath());

                    //   }
                } else if (dirs[i].isDirectory()) {
                    getCorpusFiles(dirs[i].toString());
                }
            }
        } else {
            System.err.println("here: " + dir.getAbsolutePath());
        }

    }

    public List<String> getFiles() {
        return setOfFiles;
    }

}
