/*
 * Copyright 2015 Behrang Q. Zadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.atmykitchen.common;


import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.xml.transform.OutputKeys;
import static javax.xml.transform.OutputKeys.INDENT;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import static javax.xml.transform.TransformerFactory.newInstance;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;

/**
 *
 * @author Behrang Q. Zadeh
 */
public class IOMethods {

    /**
     * read the user generated raw annotation file, remember the annotaiton file
     * are still not well-formed xml
     *
     * @param xmlPath input user generated annotaiton file path
     * @return the content of annotation file in a String
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String readFile(String xmlPath) throws FileNotFoundException, IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(xmlPath))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }
        return sb.toString();
    }

    /**
     * To generate final pretty print XML file
     * @param xmlAnnotation generated DOM from annotation file
     * @param outputPath path to write generated XML files
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws IOException
     */
    public static void writeXML(Document xmlAnnotation, String outputPath) throws TransformerConfigurationException, TransformerException, IOException {
        
        
        TransformerFactory tf = newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        FileWriter writer = new FileWriter(outputPath);
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
        transformer.transform(new DOMSource(xmlAnnotation), new StreamResult(writer));
        //String output = writer.getBuffer().toString();
        //System.out.println(output);
    }
    
    
       /**
     * To generate final pretty print XML file
     * @param xmlAnnotation generated DOM from annotation file
     * @param outputPath path to write generated XML files
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws IOException
     */
    public static void normaliseCheckWriteXML(Document xmlAnnotation, String outputPath) throws TransformerConfigurationException, TransformerException, IOException {
        TransformerFactory tf = newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
         transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
        FileWriter writer = new FileWriter(outputPath);
        
        transformer.transform(new DOMSource(xmlAnnotation), new StreamResult(writer));
        //String output = writer.getBuffer().toString();
        //System.out.println(output);
    }

    /**
     * get list of files in a given folder
     *
     * @param parentDir path to folder contains the cml files
     * @return list of file names in the specified folder
     */
    public static List<String> getAnnotationFiles(String parentDir) {
        List<String> listOfFile = new ArrayList<>();
        //FilenameFilter filter1 = new FilenameFilter() {

//            public boolean accept(File dir, String name) {
//                return !name.startsWith(".");
//            }
//        };

        File dir = new File(parentDir);
        if (dir.isDirectory()) {
            File[] dirs = dir.listFiles();
            for (File dir1 : dirs) {
                if (dir1.isFile()) {
                    listOfFile.add(dir1.getName());
                } else if (dir1.isDirectory()) {
                    out.println("Nested folder is igonored: " + dir1);
                }
            }
        }

        return listOfFile;
    }
    private static final Logger LOG = Logger.getLogger(IOMethods.class.getName());
    
   

}
