/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.atmykitchen.common;

import java.util.TreeMap;

/**
 *
 * @author Behrang QasemiZadeh <me at atmykitchen.info>
 * @param <T>
 */
public class MapCounter<T> extends TreeMap<T, Integer> {

    //TreeMap<T, Integer> counterMap 
    public MapCounter() {
        super();
    }

    public void add(T t) {
        if (this.containsKey(t)) {
            Integer get = this.get(t);
            this.put(t, ++get);
        } else {
            this.put(t, 1);
        }
    }

    public int getCount(T t) {
        if (this.containsKey(t)) {
            return this.get(t);
        } else {
            return 0;
        }
    }

    public void add(TreeMap<T, Integer> tm) {

        for (T t : tm.keySet()) {
            if (this.containsKey(t)) {
                Integer get = this.get(t) + tm.get(t);
                this.put(t, get);
            } else {
                this.put(t, 1);
            }

        }
    }
}
