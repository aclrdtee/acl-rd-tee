# README #

If you want to convert your annotation files, go to [download](https://bitbucket.org/aclrdtee/acl-rd-tee/downloads/complied_annotation_convert_ver1.zip) section and get the latest compiled version. 

Run the tool by writing the following in your command line box
$ java -jar ConvertRawAnnotations.jar
and follow the instructions promoted on the screen.

### How do I get set up and change the source codes? ###

Usual Git stuff.