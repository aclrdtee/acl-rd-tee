/*
 * Copyright 2015 Behrang QasemiZadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package scriptsformanipulatingfiles;

import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import info.atmykitchen.common.GetFiles;
import info.atmykitchen.common.XMLMethod;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.TreeSet;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This is a dummy code to Select and copy those abstracts that appear in the
 * list provided by Anne Schumann. The program is supposed to get more
 * complicated as the selection criteria for sampling the corpus is going to be
 * updated. For this moment, for the selected file, first make sure that input
 * is a valid XML file, trim the content, etc.
 *
 * @author Behrang QasemiZadeh
 */
public class GetAbstractFiles {

    private final static String rootPath = "C:\\Users\\bq\\Dropbox\\_collaborations\\_akbq\\acl_rd_tec\\";
    private static SentenceDetectorME sentenceeME;
    private static TokenizerME tokenMe;

    public static void main(String[] sugar) throws FileNotFoundException, IOException, SAXException, ParserConfigurationException, XMLStreamException {
        TreeSet<String> abstractFileNames = new TreeSet();
        BufferedReader bf = new BufferedReader(new FileReader(rootPath + "material\\selected_abstarcts.txt"));
        String line = "";
        while ((line = bf.readLine()) != null) {
            abstractFileNames.add(line.trim());
        }
        bf.close();
        System.out.println("There are " + abstractFileNames.size() + " selected file names");
        sentenceeME = new SentenceDetectorME(new SentenceModel(new File("../external_lr/en-sent.bin")));
        tokenMe = new TokenizerME(new TokenizerModel(new File("../external_lr/en-token.bin")));
        String[] sentDetect = sentenceeME.sentDetect("This is a nice day. This is another day.");
        System.out.println(sentDetect.length);
        System.out.println(tokenMe.tokenizePos(sentDetect[0]).length);
        GetFiles gf = new GetFiles();
        gf.getCorpusFiles(rootPath + "material\\_all_abstr");
        List<String> fileListAbst = gf.getFiles();
//        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
//	DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        int count = 0;
        int breakT = 0;

        for (String file : fileListAbst) {
            if (breakT++ > 400) {
                break;
            }
            String[] split = file.split("\\\\");
            // if (abstractFileNames.contains(split[split.length - 1].split("_abstr")[0])) {
            File abst = new File(file);
            Document makeDOM = XMLMethod.makeDOM(abst);
            int sectionLenth = makeDOM.getElementsByTagName("Section").getLength();
            String secTitle = makeDOM.getElementsByTagName("SectionTitle").item(0).getTextContent().trim();
            int paraLength = makeDOM.getElementsByTagName("Paragraph").getLength();
            // only those abstract files that contain one secion are allowed
            String uid = makeDOM.getElementsByTagName("Paper").item(0).getAttributes().getNamedItem("uid").getTextContent();
            if (sectionLenth == 1
                    && paraLength < 5 && paraLength > 0
                    && //!secTitle.toLowerCase().contains("introduction") 
                    secTitle.toLowerCase().contains("abstract")
                    && (abst.length() / 1024) < 3.0
                    && (uid.startsWith("P"))) {
                writeRawAbstract(file, makeDOM);
                count++;
            }
               // System.out.println(file);

            //}
        }

        System.out.println("Ci " + count);

    }

    private static void writeRawAbstract(String inputFile, Document xmlDom) throws FileNotFoundException, XMLStreamException, UnsupportedEncodingException {
        String strFile = inputFile.replace("_all_abstr", "tesing-tokenised-text");
        File f1 = new File(strFile);
        File f2 = new File(strFile);

        String parent = f2.getParent();
        File pFile = new File(parent);
        if (!pFile.exists()) {
            pFile.mkdirs();

        }
        OutputStream outputStream = new FileOutputStream(new File(strFile));
        
        XMLOutputFactory newInstance = XMLOutputFactory.newInstance();
        IndentingXMLStreamWriter out = new IndentingXMLStreamWriter(newInstance.createXMLStreamWriter(outputStream));
        out.setIndentStep("  ");
//   XMLStreamWriter out =     newInstance.createXMLStreamWriter(
//           new OutputStreamWriter(outputStream, "utf-8"));

        Node paperTag = xmlDom.getElementsByTagName("Paper").item(0);
        String uid = paperTag.getAttributes().getNamedItem("uid").getTextContent();
        out.writeStartDocument();
        out.writeStartElement("Paper");
        out.writeAttribute("acl-id", uid);

        String title = xmlDom.getElementsByTagName("Title").item(0).getTextContent().trim();
        String secTitle = xmlDom.getElementsByTagName("SectionTitle").item(0).getTextContent().trim();
        out.writeStartElement("Title");
        out.writeCharacters(title);
        out.writeEndElement();
        out.writeStartElement("Section");
        out.writeStartElement("SectionTitle");
        out.writeCharacters(secTitle);
        out.writeEndElement();
        
        NodeList paraElement = xmlDom.getElementsByTagName("Paragraph");
        for (int i = 0; i < paraElement.getLength(); i++) {
            out.writeStartElement("Paragraph");
            String textContent = paraElement.item(i).getTextContent();
            for (String sent : sentenceeME.sentDetect(textContent)) {
                out.writeStartElement("S");
            
                Span[] tokenizePos = tokenMe.tokenizePos(sent);
//                out.setIndentStep("");
                for (Span s : tokenizePos) {
                    String substring = sent.substring(s.getStart(), s.getEnd());
                    out.writeStartElement("w");
                    out.writeCharacters(substring);
                    out.writeEndElement();
                }
         //       out.setIndentStep("  ");
                //out.writeCharacters(sent);
                out.writeEndElement();
            }
            out.writeEndElement();
        }
        out.writeEndElement();

        out.writeEndElement();
        out.writeEndDocument();

        out.close();

    }

}
